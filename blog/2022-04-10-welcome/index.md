---
slug: Rublenin Hollanda hastalığı
title: Rusya, Amerikan Emlak fiyatları, Bitcoin, ECB
authors: [phi]
tags: [rusya, ecb, enflasyon, bitcoin, emlak]
---

### Rublenin Hollanda hastalığı - Doktor derdime bir çare
Altına sabitlenen Ruble gün geçtikçe değer kazanmaya devam ediyor... Peki bu Rusyanın avantajınamı, yoksa desavantajınamı?
![Russia Ruble](./ruble.png)

### Amerikada Emlak fiyatları - Satmak için iyi bir zaman
![Bad Time to Buy a Home](./homeprice.png)

### Bitcoin - Sosyopat Dede Warren Buffet
![Bitcoin](./bitcoin.png)

### Gıda fiyatları - Yüzyılın artışı
![Food Prices](./foodprices.jpg)

### Global Enflasyon - Ne Oldum Dememeli Ne Olacağım Demeli
![Para](./para.jpeg)

### Youtube Kanalı
Bu Konularla ile ilgili olan cevapları Youtube Kanalımızda bulabilirsiniz.
<iframe src="https://www.youtube.com/embed/AFUb5xq6wXU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen width="100%"></iframe> 
